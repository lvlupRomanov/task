package mvpCalc.view;

import mvpCalc.model.IFactory;

public interface IView {
    interface View{
        void run();

        void message(String val);
        void messageError(String val);
    }

    interface Presenter extends IFactory.CallBack {
        void init(IView.View view);
        String getEnterLine();
        String getOperation();
        Integer getFirstNumber();
        Integer getSecondNumber();

        @Override
        void message(String val);
    }
}
