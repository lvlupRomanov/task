package mvpCalc;

import mvpCalc.model.Factory;
import mvpCalc.model.Pair;
import mvpCalc.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static mvpCalc.model.Constant.TAG_CALC;

public class Main {
    public static void main(String[] args) {

        new View().run();

    }
}
